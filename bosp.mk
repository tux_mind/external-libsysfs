
ifdef CONFIG_EXTERNAL_LIBSYSFS

# Targets provided by this project
.PHONY: libsysfs clean_libsysfs

# Add this to the "external" target
external: libsysfs
clean_external: clean_libsysfs

MODULE_DIR_LIBSYSFS=external/required/sysfsutils

libsysfs: setup $(BUILD_DIR)/lib/libsysfs.so
$(BUILD_DIR)/lib/libsysfs.so:
	@echo
	@echo "==== Building SysFS  Library v2.1.0 ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@cd $(MODULE_DIR_LIBSYSFS) && \
		autoreconf -fi
	[ -d $(MODULE_DIR_LIBSYSFS)/build ] || \
		mkdir $(MODULE_DIR_LIBSYSFS)/build
	@cd $(MODULE_DIR_LIBSYSFS)/build && \
		CC=$(CC) CFLAGS=$(TARGET_FLAGS)" -I../../include" \
		../configure \
		--host=$(PLATFORM_TARGET) \
		--prefix=$(BUILD_DIR) \
		--disable-klibc
	@cd $(MODULE_DIR_LIBSYSFS)/build && \
		make install
	@echo

clean_libsysfs:
	@echo "==== Clean-up SysFS Lbrary v2.1.0 ===="
	@[ ! -f $(BUILD_DIR)/lib/libsysfs.so ] || \
		rm -f $(BUILD_DIR)/lib/libsysfs*
	@cd $(MODULE_DIR_LIBSYSFS)/build && \
		make distclean
	@cd $(MODULE_DIR_LIBSYSFS) && \
		rm -rf build
	@echo

else # CONFIG_EXTERNAL_LIBSYSFS

libsysfs:
	$(warning $(MODULE_DIR_LIBSYSFS) module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_LIBSYSFS

